var ConvertLib = artifacts.require("./ConvertLib.sol");
var MetaCoin = artifacts.require("./MetaCoin.sol");
var MultiSigHealthWallet = artifacts.require("./MultiSigHealthWallet.sol");
var constants = require('../../constant.json');
console.log(constants);
module.exports = function(deployer) {
  deployer.deploy(ConvertLib);
  deployer.link(ConvertLib, MetaCoin);
  deployer.deploy(MetaCoin);
  // These are address of patient and family member. If any one wants to see the records, the need approval from both members(This is multisugnature wallet)
  deployer.deploy(MultiSigHealthWallet, ["0x2e147a0c61e53c6e44363abeea57a021aba950ce", "0x16b98a1b64f9fd6d032e824d672527c85df960e5"], 2);
};
