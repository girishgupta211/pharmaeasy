
var Web3 = require('web3');
// set your web3 object
var web3 = new Web3();
var constants = require('./constant.json');
var multiSigAddress = constants.multiSigAddress;
var multisigWalletABIJson = require('./multisigWallet/build/contracts/MultiSigHealthWallet.json')
var walletABI = multisigWalletABIJson.abi; // JSON.parse(walletString);
var walletContract = web3.eth.contract(walletABI).at(multiSigAddress);


web3.setProvider(new web3.providers.HttpProvider('http://localhost:8545'));
console.log(web3.version.api);
console.log(web3.isConnected());
console.log(web3.version.node);
var txhash = walletContract.MakeTransaction.sendTransaction('0x123', 0, "dummy", {from : web3.eth.accounts[2], gas: 210000});
// console.log(txhash);

var event = walletContract.Approved({}, function (error, result) {

  if (!error) console.log(result.args.result);
  else console.log(" Request not approved");
});
