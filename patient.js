
var Web3 = require('web3');
// set your web3 object
var web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider('http://localhost:8545'));

var constants = require('./constant.json');
var multiSigAddress = constants.multiSigAddress;
var patientAddr = constants.patientAddr;

var multisigWalletABIJson = require('./multisigWallet/build/contracts/MultiSigHealthWallet.json')
var walletABI = multisigWalletABIJson.abi; // JSON.parse(walletString);
var walletContract = web3.eth.contract(walletABI).at(multiSigAddress);


var patientABIJson = require('./health/build/contracts/patient.json')
var patientABI = patientABIJson.abi;
var patientContract = web3.eth.contract(patientABI).at(patientAddr);


console.log(web3.version.api);
console.log(web3.isConnected());
console.log(web3.version.node);



 //I have preloaded the sample files on ipfs server, so we can directly store the
 // links directly.
 //QmY9Si4SRHyTssMXN74P2SJLqDEKt1kazbhzpfuVNPA3EY   hash of prescription.gif
 //QmUhQKQ7qzpZaeeT5apEjhuyNNBfAcGpkXxStfTcLsryhc   hash of EMR.gif
 //QmUhQKQ7qzpZaeeT5apEjhuyNNBfAcGpkXxStfTcLsryhc   hash of gear.jpg


  //store data prescription data.
patientContract.storePrescriptionData.sendTransaction("QmY9Si4SRHyTssMXN74P2SJLqDEKt1kazbhzpfuVNPA3EY", {from : web3.eth.accounts[0], gas: 2100000});

var event = walletContract.NewTransaction({}, function (error, result) {
  if (!error) {
    console.log(result);
    console.log("New Transaction pending for your approval, Enter yes or no");

    process.stdin.resume();
    process.stdin.setEncoding('utf8');
    var util = require('util');
    process.stdin.on('data', function(text) {
      if (text == 'yes\n') done();
      else console.log("Access Rejected");
    });

    function done() {
      console.log('Access granted');
      //for the time being hard coded address of stored medical record.
      walletContract.ConfirmTransaction.sendTransaction(result.args.txID.toNumber(), "http://ipfs.io/ipfs/QmY9Si4SRHyTssMXN74P2SJLqDEKt1kazbhzpfuVNPA3EY", {from : web3.eth.accounts[0], gas: 2100000});
    }
    // write logic for yes and no, for me its always yes :)
    //walletContract.execute.call(result.args.txID);
  }
  else console.log("Failed NewTransaction event");
});
