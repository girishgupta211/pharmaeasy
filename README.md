## Prerequisites

* Install [node](https://nodejs.org/en/)
* Install [truffle](http://truffleframework.com/docs/getting_started/installation)
* Install [ethereum](https://github.com/ethereumjs/testrpc)
* Install [ipfs](https://ipfs.io/docs/install/)

multiSignatureWallet
====================

1. I have added three sample files and hosted on ipfs,

//I have preloaded the sample files on ipfs web, so we need not to write
// code for the time being for storing them on web.
//QmY9Si4SRHyTssMXN74P2SJLqDEKt1kazbhzpfuVNPA3EY   hash of prescription.gif
//QmUhQKQ7qzpZaeeT5apEjhuyNNBfAcGpkXxStfTcLsryhc   hash of EMR.gif
//QmUhQKQ7qzpZaeeT5apEjhuyNNBfAcGpkXxStfTcLsryhc   hash of gear.jpg

2. Doctor asks for patients prescription file
3. Transaction will be approved by patient.
4. After approval IPFS link will be shared to the doctor, http://ipfs.io/ipfs/HASH, here hash will one of as mentioned above.



Step for demo
===============


1. Start testerpc 
```sh
$ tanisha@Tani-Pavilion-Notebook:~/projects/multiSignatureWallet$ testrpc 
```

* logs
```js
EthereumJS TestRPC v4.0.1 (ganache-core: 1.0.1)

Available Accounts

(0) 0x9e92b703e080940da5d6bbaa7d12343efa54ccd9
(1) 0x5291813546f241079ed455f0c27e214e8e09a77c
(2) 0xf7313a859ba30d3c03a2ed4351de3cfab6427387
```

2. copy first 2 address in wallet/migrations/2_deploy_contracts.js.
deployer.deploy(MultiSigHealthWallet, ["0x9e92b703e080940da5d6bbaa7d12343efa54ccd9", "0x5291813546f241079ed455f0c27e214e8e09a77c"], 2);

Here first address is patient and 2nd is family member

3. Copy patient address in constant.js

4. deploy multisig 
```sh
tanisha@Tani-Pavilion-Notebook:~/projects/multiSignatureWallet/wallet$ truffle complile
tanisha@Tani-Pavilion-Notebook:~/projects/multiSignatureWallet/wallet$ truffle migrate
Using network 'development'.

Running migration: 1_initial_migration.js
  Deploying Migrations...
  Migrations: 0x90660f473f093c84bff73cc9506ea80866ff3571
Saving successful migration to network...
Saving artifacts...
Running migration: 2_deploy_contracts.js
  Deploying ConvertLib...
  ConvertLib: 0xf3a7009706495b4886034c16884a1c022f846c19
  Linking ConvertLib to MetaCoin
  Deploying MetaCoin...
  MetaCoin: 0x5aa16fbe44c56874592f3124a4dce5d8187d01f2
  Deploying MultiSigHealthWallet...
  MultiSigHealthWallet: `0x829c97beb9a0ee10a10572ad9c637769642d3648`
Saving successful migration to network...
Saving artifacts...
```

5. Copy MultiSigHealthWallet address (contract address) and paste it in constant.js

6. run patient.js and family.js

7. run doctor.js

8. Then there will be prompt on patient.js and family.js for yes/no.
if both says yes, then doctor will be able to see the patient data(ipfs link)

```sh
tanisha@Tani-Pavilion-Notebook:~/projects/multiSignatureWallet$ node family.js 
New Transaction pending for your approval, Enter yes or no
yes
Access granted

tanisha@Tani-Pavilion-Notebook:~/projects/multiSignatureWallet$ node patient.js 
New Transaction pending for your approval, Enter yes or no
yes
Access granted


link will appear on doctor window.
tanisha@Tani-Pavilion-Notebook:~/projects/multiSignatureWallet$ node doctor.js 
http://ipfs.io/ipfs/QmY9Si4SRHyTssMXN74P2SJLqDEKt1kazbhzpfuVNPA3EY
```
